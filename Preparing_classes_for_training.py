#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import os
import shutil
import numpy as np


def angles_categories_combination(folder_path,rotated,rotated1,angle,angle_1,angle_):
    '''
    Function to generate classes to train the classification. 
    :param categories : List of categories needed for training
    :return: classes generated
    '''
    #only this angles should be used and other angles are eliminated as images are getting repeated

    categories=category_name(folder_path)

    classes_dir=[]

    for r in categories:
        for m in rotated1:
            for k in angle:
                cat_rot= str(r)+'_'+'x'+'_'+str(m)+'_'+'y_'+str(k)+'_z_0'
                if cat_rot not in classes_dir:
                    classes_dir.append(cat_rot)
            for kkk in angle_:
                if kkk!=90:
                    cat_rot2= str(r)+'_'+'x'+'_'+str(0)+'_'+'y_'+str(m)+'_z_'+str(kkk)
                    if cat_rot2 not in classes_dir:
                        classes_dir.append(cat_rot2)
                else:
                    if m not in angle_1:
                        cat_rot2= str(r)+'_'+'x'+'_'+str(0)+'_'+'y_'+str(m)+'_z_'+str(kkk)

                        if cat_rot2 not in classes_dir:
                            classes_dir.append(cat_rot2)
        for mm in rotated:
            for kk in angle_:
                if kk==90 and mm==180:
                    pass
                else:
                    cat_rot1= str(r)+'_'+'x'+'_'+str(mm)+'_'+'y_'+str(0)+'_z_'+str(kk)
                    if cat_rot1 not in classes_dir:
                        classes_dir.append(cat_rot1)
        for mmm in rotated1:
            for kkkk in angle:
                if mmm not in angle:

                    cat_rot3= str(r)+'_'+'x'+'_'+str(kkkk)+'_'+'y_'+str(mmm)+'_z_'+str(0)
                    if cat_rot3 not in classes_dir:
                        classes_dir.append(cat_rot3)
        for m1 in rotated1:
            for k1 in angle:
                if m1 not in angle_1:
                    cat_rot4= str(r)+'_'+'x'+'_'+str(k1)+'_'+'y_'+str(0)+'_z_'+str(m1)
                    if cat_rot4 not in classes_dir:
                        classes_dir.append(cat_rot4)
    return classes_dir

def making_dir(classes_dir,folder_class): 
    '''
    Function to create seperate folders for each class
    :param classes_dir : List of classes needed for training
    :return: None
    '''
    for cls in classes_dir:
         if not os.path.exists(folder_class + '/'+cls):
            os.makedirs(folder_class + '/'+cls)
            
def category_name(folder_path):
    '''
    Function to create a list of categories needed for training
    :param folder_path : path to the folder where the data presents
    :return categories: list of categories name 
    '''
    categories=[]
    for filename in os.listdir(folder_path):
        if filename not in categories:
            categories.append(filename)
    return categories

def combing_angles():
    '''
    Function to create a matrix type of array inorder to combine the nearby 5th angle
    :return matrix type of array
    '''
    Ang=np.arange(-5,355,5)
    Ang = Ang.reshape(24,3)
    Ang[Ang==-5]=355
    return Ang

def copying_data_with_seperate_class(folder_path,folder_class,rotated,rotated1,angle,angle_1,angle_):
    '''
    Function is to copy the data from folder_data to  folderr 
    :param folder_path : path to the folder where the data presents
    :param angle : list of particular angle
    :param folderr : folder where the image should be copied
    '''    
    categories=category_name(folder_path)
    
    for data in categories:
        for category in os.listdir(folder_path+'/'+data):
            for filename in os.listdir(folder_path+'/'+data +"/"+ category + "/"):
                file_ =  folder_path+'/'+data +"/"+category + "/" + filename
                copying_data_with_angle(file_,data,folder_path,folder_class,rotated,rotated1,angle,angle_1,angle_)
                
                
def copying_data_with_angle(file_,data,folder_path,folder_class,rotated,rotated1,angle,angle_1,angle_):
    '''
    Function is to copy the images from folder where data presents to the particular class folder 
    :param folder_path : path to the folder where the data presents
    :param file_ : location where the images present
    :param data : each categories
    :param folderr : folder where the image should be copied
    '''   
    categories=category_name(folder_path)
    classes_dir=angles_categories_combination(folder_path,rotated,rotated1,angle,angle_1,angle_)
    Ang=combing_angles()
    if file_.endswith('.png'):
        for i in Ang:
            for k in angle:
                if  file_.endswith('_'+'x'+'_'+str(k)+'_'+'y'+'_'+str(i[0])+'_'+'z_0.png') or file_.endswith('_'+'x'+'_'+str(k)+'_'+'y'+'_'+str(i[1])+'_'+'z_0.png') or file_.endswith('_'+'x'+'_'+str(k)+'_'+'y'+'_'+str(i[2])+'_'+'z_0.png'):
                    if (data+'_x'+'_'+str(k)+'_'+'y'+'_'+str(i[1])+'_'+'z_0') in classes_dir:
                        shutil.copy(file_,folder_class+"/"+ data+'_x'+'_'+str(k)+'_'+'y'+'_'+str(i[1])+'_'+'z_0' +'/')
                if  file_.endswith('_'+'x'+'_'+str(i[0])+'_y_'+str(0)+'_'+'z_'+str(k)+'.png') or file_.endswith('_'+'x'+'_'+str(i[1])+'_y_'+str(0)+'_'+'z_'+str(k)+'.png') or file_.endswith('_'+'x'+'_'+str(i[2])+'_'+'y'+'_'+str(0)+'_'+'z_'+str(k)+'.png'):
                    if ( data+'_x'+'_'+str(i[1])+'_'+'y'+'_'+str(0)+'_'+'z_' +str(k)) in classes_dir:
                        shutil.copy(file_,folder_class+"/"+ data+'_x'+'_'+str(i[1])+'_'+'y'+'_'+str(0)+'_'+'z_' +str(k)+'/')
                if  file_.endswith('_'+'x'+'_'+str(i[0])+'_y_'+str(k)+'_'+'z_'+str(0)+'.png') or file_.endswith('_'+'x'+'_'+str(i[1])+'_y_'+str(k)+'_'+'z_'+str(0)+'.png') or file_.endswith('_'+'x'+'_'+str(i[2])+'_'+'y'+'_'+str(k)+'_'+'z_'+str(0)+'.png'):
                    if ( data+'_x'+'_'+str(i[1])+'_'+'y'+'_'+str(k)+'_'+'z_' +str(0)) in classes_dir:
                        shutil.copy(file_,folder_class+"/"+ data+'_x'+'_'+str(i[1])+'_'+'y'+'_'+str(k)+'_'+'z_' +str(0)+'/')
                if  file_.endswith('_'+'x'+'_'+str(k)+'_y_'+str(0)+'_'+'z_'+str(i[0])+'.png') or file_.endswith('_'+'x'+'_'+str(k)+'_y_'+str(0)+'_'+'z_'+str(i[1])+'.png') or file_.endswith('_'+'x'+'_'+str(k)+'_y_'+str(0)+'_'+'z_'+str(i[2])+'.png'):
                    if ( data+'_x'+'_'+str(k)+'_y_'+str(0)+'_'+'z_'+str(i[1])) in classes_dir:
                        shutil.copy(file_,folder_class+"/"+ data+'_x'+'_'+str(k)+'_y_'+str(0)+'_'+'z_'+str(i[1])+'/')
                if  file_.endswith('_'+'x'+'_'+str(0)+'_'+'y'+'_'+str(i[0])+'_'+'z_'+str(k)+'.png') or file_.endswith('_'+'x'+'_'+str(0)+'_'+'y'+'_'+str(i[1])+'_'+'z_'+str(k)+'.png') or file_.endswith('_'+'x'+'_'+str(0)+'_'+'y'+'_'+str(i[2])+'_'+'z_'+str(k)+'.png'):
                    if ( data+'_x'+'_'+str(0)+'_'+'y'+'_'+str(i[1])+'_'+'z_'+str(k)) in classes_dir:
                        shutil.copy(file_,folder_class+"/"+ data+'_x'+'_'+str(0)+'_'+'y'+'_'+str(i[1])+'_'+'z_'+str(k)+'/')


# In[ ]:


def create_classes():
    '''
    Function is create classes based on the images inorder to use for training 
    :param folder_path : path to the folder where the data presents
    :param folder_class : specific location where classes need to be created
    :param folderr : folder where the image should be copied
    '''    
    folder_path=r'D:\Anything_world_Env\Anything_world\rotation_and_scaling\data\anything_world\cat_trial'
    folder_class=r'F:\Categoriesss'
    #folderr=r'F:'
    rotated = list(range(15,360,15))
    rotated1= list(range(0,360,15))
    angle_1=[90,180,270]
    angle_=[90,270]
    angle=[0,90,180,270]
    classes_dir=angles_categories_combination(folder_path,rotated,rotated1,angle,angle_1,angle_)
    folder_creating=making_dir(classes_dir,folder_class)
    copying_data_with_seperate_class(folder_path,folder_class,rotated,rotated1,angle,angle_1,angle_)
create_classes()


# In[ ]:




