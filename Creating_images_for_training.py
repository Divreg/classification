#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import vtk
from vtk import ( vtkPolyDataMapper, vtkActor, vtkRenderer,
        vtkRenderWindow, vtkWindowToImageFilter, vtkPNGWriter)
import os
import shutil



def rotation_x_90angle_y_5angle_z_0(angles,angles1,file_):
    
    '''
    Function is to rotate the model in y and x-axis in different angle depending upon the criteria given.
    :param file_: path to the folder where the input model is present.
    :angles: angle to rotate in y-axis
    :angles1: angle to rotate in x-axis
    :return: None
    '''
    for ii in angles:
        for iii in angles1:
            #in-built function to read the obj file
            reader = vtk.vtkOBJReader()
            reader.SetFileName(file_)
            reader.Update()
            transform = vtk.vtkTransform()

            if ii not in [90,180,270]:
            #models rotate in Y-axis and then X-axis
                transform.RotateY(ii)
                transform.RotateX(iii)
                transformFilter=vtk.vtkTransformPolyDataFilter()
                transformFilter.SetTransform(transform)
                transformFilter.SetInputConnection(reader.GetOutputPort())
                transformFilter.Update()
                Mapper2 = vtk.vtkPolyDataMapper()
                Mapper2.SetInputConnection(transformFilter.GetOutputPort())
                ren = vtk.vtkRenderer()
                 #Add  Render Window
                renWin = vtk.vtkRenderWindow()
                #render screen will appear if set to 0
                renWin.SetOffScreenRendering(1)
                renWin.AddRenderer(ren)
                actor = vtk.vtkActor()
                actor.SetMapper(Mapper2)
                # setting the image color
                actor.GetProperty().SetColor(1,1,1) # (R,G,B)
                # assign actor to the renderer
                ren.AddActor(actor)
                renWin.Render()
                screenshot_batch_x_90angle_y_5angle_z_0(file_,ii,iii,renWin)
                


def rotation_x_5angle_y_90angle_z_0(angles,angles1,file_):
    '''
    Function is to rotate the model in y and x-axis in different angle depending upon the criteria given.
    :param file_: path to the folder where the input model is present.
    :angles: angle to rotate in x-axis
    :angles1: angle to rotate in y-axis
    :return: None
    '''
    for ii in angles:
        for iii in angles1:
            reader = vtk.vtkOBJReader()
            reader.SetFileName(file_)
            reader.Update()
            transform = vtk.vtkTransform()

            if ii not in [90,180,270]:
            #models rotate in Y-axis and then X-axis
                transform.RotateY(iii)
                transform.RotateX(ii)
                transformFilter=vtk.vtkTransformPolyDataFilter()
                transformFilter.SetTransform(transform)
                transformFilter.SetInputConnection(reader.GetOutputPort())
                transformFilter.Update()
                Mapper2 = vtk.vtkPolyDataMapper()
                Mapper2.SetInputConnection(transformFilter.GetOutputPort())
                ren = vtk.vtkRenderer()
                 # Render Window
                renWin = vtk.vtkRenderWindow()
                renWin.SetOffScreenRendering(1)
                renWin.AddRenderer(ren)
                actor = vtk.vtkActor()
                actor.SetMapper(Mapper2)
                # setting the image color
                actor.GetProperty().SetColor(1,1,1) # (R,G,B)

                # assign actor to the renderer
                ren.AddActor(actor)
                #ren.SetBackground(1, 0.5, 1)
                renWin.Render()
                screenshot_batch_x_5angle_y_90angle_z_0(file_,ii,iii,renWin)

                
def rotation_x_5angle_y_0_z_90angle(angles,angles2,file_):
    '''
    Function is to rotate the model in z and x-axis in different angle depending upon the criteria given.
    :param file_: path to the folder where the input model is present.
    :angles: angle to rotate in x-axis
    :angles2: angle to rotate in z-axis
    :return: None
    '''
    for ii in angles:
        for iii in angles2:
            reader = vtk.vtkOBJReader()
            reader.SetFileName(file_)
            reader.Update()
            transform = vtk.vtkTransform()
            if not ii==0:
            #models rotate in Y-axis and then X-axis
                transform.RotateZ(iii)
                transform.RotateX(ii)
                transformFilter=vtk.vtkTransformPolyDataFilter()
                transformFilter.SetTransform(transform)
                transformFilter.SetInputConnection(reader.GetOutputPort())
                transformFilter.Update()
                Mapper2 = vtk.vtkPolyDataMapper()
                Mapper2.SetInputConnection(transformFilter.GetOutputPort())
                ren = vtk.vtkRenderer()
                 # Render Window
                renWin = vtk.vtkRenderWindow()
                renWin.SetOffScreenRendering(1)
                renWin.AddRenderer(ren)
                actor = vtk.vtkActor()
                actor.SetMapper(Mapper2)
                # setting the image color
                actor.GetProperty().SetColor(1,1,1) # (R,G,B)

                # assign actor to the renderer
                ren.AddActor(actor)
                #ren.SetBackground(1, 0.5, 1)
                renWin.Render()
               
                screenshot_batch_x_5angle_y_0_z_90angle(file_,ii,iii,renWin)

            
                
def rotation_x_0_y_5angle_z_90angle(angles,angles2,file_):
    '''
    Function is to rotate the model in z and y-axis in different angle depending upon the criteria given.
    :param file_: path to the folder where the input model is present.
    :angles: angle to rotate in y-axis
    :angles2: angle to rotate in z-axis
    :return: None
    '''
    for ii in angles:
        for iii in angles2:
            reader = vtk.vtkOBJReader()
            reader.SetFileName(file_)
            reader.Update()
            transform = vtk.vtkTransform()
            if iii!=0:
                transform.RotateZ(iii)
                transform.RotateY(ii)
                transformFilter=vtk.vtkTransformPolyDataFilter()
                transformFilter.SetTransform(transform)
                transformFilter.SetInputConnection(reader.GetOutputPort())
                transformFilter.Update()
                Mapper2 = vtk.vtkPolyDataMapper()
                Mapper2.SetInputConnection(transformFilter.GetOutputPort())
                ren = vtk.vtkRenderer()
                 # Render Window
                renWin = vtk.vtkRenderWindow()
                renWin.SetOffScreenRendering(1)
                renWin.AddRenderer(ren)
                actor = vtk.vtkActor()
                actor.SetMapper(Mapper2)
                # setting the image color
                actor.GetProperty().SetColor(1,1,1) # (R,G,B)

                # assign actor to the renderer
                ren.AddActor(actor)
                #ren.SetBackground(1, 0.5, 1)
                renWin.Render()
                screenshot_batch_x_0_y_5angle_z_90angle(file_,ii,iii,renWin)

                
def rotation_x_90angle_y_0_z_5angle(angles,angles1,file_):
    '''
    Function is to rotate the model in z and x-axis in different angle depending upon the criteria given.
    :param file_: path to the folder where the input model is present.
    :angles: angle to rotate in z-axis
    :angles2: angle to rotate in x-axis
    :return: None
    '''
    for ii in angles:
        for iii in angles1:
            reader = vtk.vtkOBJReader()
            reader.SetFileName(file_)
            reader.Update()
            transform = vtk.vtkTransform()
            if ii!=180:
                transform.RotateZ(ii)
                transform.RotateX(iii)
                transformFilter=vtk.vtkTransformPolyDataFilter()
                transformFilter.SetTransform(transform)
                transformFilter.SetInputConnection(reader.GetOutputPort())
                transformFilter.Update()
                Mapper2 = vtk.vtkPolyDataMapper()
                Mapper2.SetInputConnection(transformFilter.GetOutputPort())
                ren = vtk.vtkRenderer()
                 # Render Window
                renWin = vtk.vtkRenderWindow()
                renWin.SetOffScreenRendering(1)
                renWin.AddRenderer(ren)
                actor = vtk.vtkActor()
                actor.SetMapper(Mapper2)
                # setting the image color
                actor.GetProperty().SetColor(1,1,1) # (R,G,B)

                # assign actor to the renderer
                ren.AddActor(actor)
                #ren.SetBackground(1, 0.5, 1)
                renWin.Render()               
                screenshot_batch_x_90angle_y_0_z_5angle(file_,ii,iii,renWin)
                

def screenshot_batch_x_90angle_y_5angle_z_0(file_,ii,iii,renWin):  
    '''
    Function is to take the screenshot of the rotated models and save the model as .png
    :param file_: path to the folder where the input model is present.
    :renWin: rotated model in the rendering window
    :return: None
    '''
    windowToImageFilter = vtkWindowToImageFilter()
    windowToImageFilter.SetInput(renWin)
    windowToImageFilter.Update()
    #function used to write the input file as image
    writer = vtkPNGWriter()
    writer.SetFileName(file_[:-4]+ '_x_'+str(iii)+'_y_'+str(ii)+'_z_'+str(0)+ '.png')
    writer.SetInputConnection(windowToImageFilter.GetOutputPort())
    writer.Write()
    
def screenshot_batch_x_5angle_y_90angle_z_0(file_,ii,iii,renWin): 
    '''
    Function is to take the screenshot of the rotated models and save the model as .png
    :param file_: path to the folder where the input model is present.
    :renWin: rotated model in the rendering window
    :return: None
    '''
    windowToImageFilter = vtkWindowToImageFilter()
    windowToImageFilter.SetInput(renWin)
    windowToImageFilter.Update()

    writer = vtkPNGWriter()
    writer.SetFileName(file_[:-4]+ '_x_'+str(ii)+'_y_'+str(iii)+'_z_'+str(0)+ '.png')        
    writer.SetInputConnection(windowToImageFilter.GetOutputPort())
    writer.Write()
    
def screenshot_batch_x_5angle_y_0_z_90angle(file_,ii,iii,renWin): 
    '''
    Function is to take the screenshot of the rotated models and save the model as .png
    :param file_: path to the folder where the input model is present.
    :renWin: rotated model in the rendering window
    :return: None
    '''
    windowToImageFilter = vtkWindowToImageFilter()
    windowToImageFilter.SetInput(renWin)
    windowToImageFilter.Update()

    writer = vtkPNGWriter()
    writer.SetFileName(file_[:-4]+ '_x_'+str(ii)+'_y_'+str(0)+'_z_'+str(iii)+ '.png')        
    writer.SetInputConnection(windowToImageFilter.GetOutputPort())
    writer.Write()
    
def screenshot_batch_x_0_y_5angle_z_90angle(file_,ii,iii,renWin):  
    '''
    Function is to take the screenshot of the rotated models and save the model as .png
    :param file_: path to the folder where the input model is present.
    :renWin: rotated model in the rendering window
    :return: None
    '''
    windowToImageFilter = vtkWindowToImageFilter()
    windowToImageFilter.SetInput(renWin)
    windowToImageFilter.Update()

    writer = vtkPNGWriter()
    writer.SetFileName(file_[:-4]+ '_x_'+str(0)+'_y_'+str(ii)+'_z_'+str(iii)+ '.png')
    writer.SetInputConnection(windowToImageFilter.GetOutputPort())
    writer.Write()
    
def screenshot_batch_x_90angle_y_0_z_5angle(file_,ii,iii,renWin): 
    '''
    Function is to take the screenshot of the rotated models and save the model as .png
    :param file_: path to the folder where the input model is present.
    :renWin: rotated model in the rendering window
    :return: None
    '''
    windowToImageFilter = vtkWindowToImageFilter()
    windowToImageFilter.SetInput(renWin)
    windowToImageFilter.Update()

    writer = vtkPNGWriter()
    writer.SetFileName(file_[:-4]+ '_x_'+str(iii)+'_y_'+str(0)+'_z_'+str(ii)+ '.png')
    writer.SetInputConnection(windowToImageFilter.GetOutputPort())
    writer.Write()

def training_images_classification(folder):
    
    '''
    Function used for creating images for classification by rotating the model in x,y and z depending upon the criteria given.
    :param folder: path to the folder where the input model is present. The model should scaled and moved to origin.
    :return: None
    '''
    angles = list(range(0,360,5))
    angles2=[90,270]
    angles3=[0,270]
    angles1=[0,90,180,270]
    for category in os.listdir(folder):
        for filename in os.listdir(folder  +"/"+ category + "/"):
            file_path =  folder  +"/"+category + "/" + filename
            if file_path.endswith('scaled_origin.obj'):
                rotation_x_90angle_y_5angle_z_0(angles,angles1,file_path)
                rotation_x_5angle_y_90angle_z_0(angles,angles1,file_path)
                rotation_x_5angle_y_0_z_90angle(angles,angles2,file_path)
                rotation_x_0_y_5angle_z_90angle(angles,angles2,file_path)
                rotation_x_90angle_y_0_z_5angle(angles,angles1,file_path)
                

training_images_classification(r'D:\Anything_world\whole_pipeline\model-segmentation-pipeline\aaa\1Newfolder\a')

