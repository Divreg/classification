#!/usr/bin/env python
# coding: utf-8

# In[ ]:

#from paraview import vtk
#import vtk
from paraview.simple import *
from vtk import *
import vtk
#from vtk import ( vtkPolyDataMapper, vtkActor, vtkRenderer,
#        vtkRenderWindow, vtkWindowToImageFilter, vtkPNGWriter)
import os
import glob
from math import *
from os.path import join
from os import listdir


# In[ ]:
#cone = Cone()
#Show()
Show(Cone())
#folder_path=r'/home/automation/classification/trial'
#sub_folder='trial'


# In[ ]:


def rotated_image(folder):
    angles = list(range(0,360,15))
    for category in os.listdir(folder):
        for filename in os.listdir(folder +"/"+ category + "/"):
            file_ =  folder  +"/"+category + "/" + filename
            if file_.endswith('scaled_origin.obj') :
            
                for i in angles:
                    
                    reader = vtk.vtkOBJReader()

                    reader.SetFileName(file_)

                    reader.Update()
                    transform = vtk.vtkTransform()

                
                    transform.RotateWXYZ(i,0,1,0)
                    transformFilter=vtk.vtkTransformPolyDataFilter()
                    transformFilter.SetTransform(transform)
                    transformFilter.SetInputConnection(reader.GetOutputPort())
                    transformFilter.Update()
                    Mapper2 = vtk.vtkPolyDataMapper()
                    if vtk.VTK_MAJOR_VERSION <= 5:
                        Mapper2.SetInput(transformFilter.GetOutput())
                    else:
                        Mapper2.SetInputConnection(transformFilter.GetOutputPort())
                    ren = vtk.vtkRenderer()
                    renWin = vtk.vtkRenderWindow()
                    renWin.SetOffScreenRendering(1)
                    renWin.AddRenderer(ren)
                    actor = vtk.vtkActor()
                    actor.SetMapper(Mapper2)
 

                # color rotated 
                    actor.GetProperty().SetColor(1,1,1) # (R,G,B)
 
                # assign actor to the renderer

                    ren.AddActor(actor)
                #ren.SetBackground(1, 1, 1)
                    renWin.Render()

                    windowToImageFilter = vtkWindowToImageFilter()
                    windowToImageFilter.SetInput(renWin)
                    windowToImageFilter.Update()

                    writer = vtkPNGWriter()
                    writer.SetFileName(file_[:-4]+ '_y_'+str(i)+ '.png')
                    writer.SetInputConnection(windowToImageFilter.GetOutputPort())
                    writer.Write()
    


# In[ ]:


class Image_generating:
    img = rotated_image(folder_path)
output_images=Image_generating
output_images()

