from support_file_for_images import automation_support_images
from automation_classification_rps import classification_rotation_prefab


def classify_models(unsegmented_folder_path, trained_model_path):

    # Prepare model images
    automation_support_images(unsegmented_folder_path)

    # Classify models
    return classification_rotation_prefab(unsegmented_folder_path, trained_model_path)

