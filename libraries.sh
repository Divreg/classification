virtualenv --python python3 envs/classification
source envs/classification/bin/activate
pip3 install tensorflow-gpu==2.2.0
pip3 install keras
pip3 install pandas
pip3 install os
pip3 install vtk
virtualenv --python python3 envs/segmentation
source envs/segmentation/bin/activate
pip3 install tensorflow-gpu==1.13.1
pip3 install keras
pip3 install pandas
pip3 install os
pip3 install scikit-learn
pip3 install scipy

