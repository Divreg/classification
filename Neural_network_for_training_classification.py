#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from tensorflow.keras.utils import to_categorical
import numpy as np
import os
import cv2
import random
import keras
from keras.models import Sequential,Input,Model
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.layers.normalization import BatchNormalization
from keras.layers.advanced_activations import LeakyReLU
    


def train_test_split(folder):
    '''
    Function is to split the training and testing data as 85% and 15% respectively
    :param folder : path to the folder where classes present
    :return file_path_category: full path to the class
    :return train_File_path: path to the image for training set
    :return test_File_path : path to the image for testing set
    :return total_classes : total count of the classes present during training
    '''
    # Creating partitions of the data after shuffeling
    test_ratio = 0.15
    file_path_category = []
    train_File_path=[]
    test_File_path=[]
    count=0
    for cls in os.listdir(folder):
        # Folder to copy images from
        src = folder+'/'+cls
        count+=1
        allFileNames = os.listdir(src)
        np.random.shuffle(allFileNames)
        train_FileNames, test_FileNames = np.split(np.array(allFileNames),[int(len(allFileNames)* (1 - test_ratio))])
        file_path_category.append(cls)
        for name in train_FileNames:
            train_File_path .append(src+'/' +  name)
        for name in test_FileNames:
            test_File_path .append(src+'/' +  name)
    total_classes=count
    return file_path_category,train_File_path,test_File_path,total_classes


def y_test_preparation(folder):
    '''
    Function is to create labels using one hot encoder
    :param folder : path to the folder where classes present
    : file_path_category: full path to the class
    : train_File_path: path to the image for training set
    : test_File_path : path to the image for testing set
    : total_classes : total count of the classes present during training
    : return Y_test : labelled output of the testing data
    '''
    file_path_category,_,test_File_path,total_classes=train_test_split(folder)
    y_test=[]
    for i in test_File_path:
        k=i.split('/')[1]
        if k in file_path_category:
            y_test.append(file_path_category.index(k))
    y_test=np.asarray(y_test)
    y_test=y_test.astype(int)
    Y_test = to_categorical(y_test, total_classes,dtype='int8')
    return Y_test

def x_test_preparation(folder):
    '''
    Function is to convert images to numpy array
    :param folder : path to the folder where classes present
    : test_File_path : path to the image for testing set
    : return X_test  : converted output
    '''
    _,_,test_File_path,_=train_test_split(folder)
    data = []
    # loop over the test_File_path
    for imagePath in test_File_path:
    # load the image, and resize it to be a fixed 30*30 pixels, ignoring aspect ratio
        image = cv2.imread(imagePath,0)
        image = cv2.resize(image, (30, 30))

    # update the data lists
        data.append(image)
    data1 = np.array(data, dtype='uint8')
    X_test = data1.reshape(data1.shape[0],30, 30 ,1)
    return X_test


def y_train_preparation(folder):
    '''
    Function is to create labels using one hot encoder
    :param folder : path to the folder where classes present
    : file_path_category: full path to the class
    : train_File_path: path to the image for training set
    : test_File_path : path to the image for testing set
    : total_classes : total count of the classes present during training
    : return Y_train  : labelled output of the training data
    '''
    file_path_category,train_File_path,_,total_classes=train_test_split(folder)
    y_train=[]
    for i in train_File_path:
        k=i.split('/')[1]
        if k in file_path_category:
            y_train.append(file_path_category.index(k))
    y_train=np.asarray(y_train)
    y_train=y_train.astype(int)
    Y_train = to_categorical(y_train, total_classes,dtype='int8')
    return Y_train

def x_train_preparation(folder):
    '''
    Function is to convert images to numpy array
    :param folder : path to the folder where classes present
    : file_path_category: full path to the class
    : train_File_path: path to the image for training set
    : return X_train  : converted output
    '''
    _,train_File_path,_,_=train_test_split(folder)
    data3 = []
    # loop over the image paths
    for imagePath1 in train_File_path:
    # load the image, and resize it to be a fixed 30*30 pixels, ignoring aspect ratio
        image = cv2.imread(imagePath1,0)
        image = cv2.resize(image, (30, 30))

    # update the data lists
        data3.append(image)
    data2 = np.array(data3, dtype='uint8')
    X_train = data2.reshape(data2.shape[0],30, 30 ,1)
    return X_train


def neural_network_model(folder):
    '''
    Defining the Neural network with 3 layers with maxpooling and droupout 
    :param folder : path to the folder where classes present
    : return model  : neural network model used for the training
    '''
    
    _,_,_,total_classes=train_test_split(folder)
    model = Sequential()
    model.add(Conv2D(32, kernel_size=(3, 3),activation='linear',padding='same',input_shape=(30, 30,1)))
    model.add(LeakyReLU(alpha=0.1))
    model.add(MaxPooling2D((2, 2),padding='same'))
    model.add(Dropout(0.25))
    #model.add(Dropout(0.5))
    model.add(Conv2D(64, (3, 3), activation='linear',padding='same'))
    model.add(LeakyReLU(alpha=0.1))
    model.add(MaxPooling2D(pool_size=(2, 2),padding='same'))
    model.add(Dropout(0.25))
    #model.add(Dropout(0.5))

    model.add(Conv2D(128, (3, 3), activation='linear',padding='same'))
    model.add(LeakyReLU(alpha=0.1))                  
    model.add(MaxPooling2D(pool_size=(2, 2),padding='same'))
    model.add(Dropout(0.4))
    #model.add(Dropout(0.5))

    model.add(Flatten())
    model.add(Dense(128, activation='linear'))
    model.add(LeakyReLU(alpha=0.1))           
    #model.add(Dropout(0.3))
    model.add(Dropout(0.5))

    model.add(Dense(total_classes, activation='softmax'))
    model.summary()
    return model


def fit_save_model(epochs,batch_size,folder):
    '''
    Function is to fit and compile the model to get trained model and save every 5th epoch
    :param folder : path to the folder where classes present
    : return: None
    '''
    model=neural_network_model(folder)
    y_test,x_test=testing_data_conversion(folder)
    y_train,x_train=training_data_conversion(folder)

    for epoch in range(epochs):
        print('\n>>> Training for the epoch %d/%d ...' % (epoch+1, epochs))

        model.compile(loss=keras.losses.categorical_crossentropy, optimizer=keras.optimizers.Adam(),metrics=['accuracy'])
        history=model.fit(x_train, y_train, batch_size=batch_size,verbose=1,validation_data=(x_test, y_test))


        if (epoch+1) % 5 == 0:
            cp_filename = model.save(os.path.join(MODEL_STORAGE_PATH, 'epoch1_' + str(epoch+1)+'.h5'))
        

def testing_data_conversion(folder):
    '''
    Function is to convert training data into labels and arrays
    :param folder : path to the folder where classes present
    : return: labels and arrays correspond to each image
    '''
    y_test=y_test_preparation(folder)
    x_test=x_test_preparation(folder)
    return y_test,x_test
    
def training_data_conversion(folder):
    '''
    Function is to convert testing data into labels and arrays
    :param folder : path to the folder where classes present
    : return: labels and arrays correspond to each image
    '''
    y_train=y_train_preparation(folder)
    x_train=x_train_preparation(folder)
    return y_train,x_train

def trained_nn():
    '''
    Function is to train the neural network for classification for the classes given 
    This is the 2D image classification
    : return: None
    '''
    MODEL_STORAGE_PATH=r'F:/model_new_512'
    batch_size = 512
    epochs = 100
    folder=r'F:\Categoriesss'
   
    fit_save_model(epochs,batch_size,folder)

trained_nn()    


# In[ ]:


def create_classes():
    '''
    Function is create classes based on the images inorder to use for training 
    :param folder_path : path to the folder where the data presents
    :param folder_class : specific location where classes need to be created
    :param folderr : folder where the image should be copied
    '''    
    folder_path=r'D:\Anything_world_Env\Anything_world\rotation_and_scaling\data\anything_world\cat_trial'
    folder_class=r'F:\Categoriesss'
    #folderr=r'F:'
    rotated = list(range(15,360,15))
    rotated1= list(range(0,360,15))
    angle_1=[90,180,270]
    angle_=[90,270]
    angle=[0,90,180,270]
    classes_dir=angles_categories_combination(folder_path,rotated,rotated1,angle,angle_1,angle_)
    folder_creating=making_dir(classes_dir,folder_class)
    copying_data_with_seperate_class(folder_path,folder_class,rotated,rotated1,angle,angle_1,angle_)
create_classes()


# In[ ]:




