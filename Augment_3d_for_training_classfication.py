#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import numpy as np
import math
import random
import os


def load_model(path):
    '''
    Function used for loading vertices of a 3D model from the .obj file
    :param path: path to the file containing definition of 3D model
    :return: numpy array containing vertices of 3D model [nx3]
    '''
    ftp = open(path, 'r')
    file = ftp.readlines()

    vertices = []
    for line in file:
        if line.startswith('v '):
            point = np.float32(line.strip().split(' ')[1:4])
            vertices.append(point)

    ftp.close()

    return np.array(vertices)


def save_model(original_path, path, vertices):
    '''
    Function used for saving vertices of a 3D model (together with the faces and normals definitions from original file) as an .obj file
    :param original_path: path to the file containing definition of 3D model that was processed
    :param path: path to the file where new modified model is to be saved
    :param vertices: numpy array containing vertices of 3D model [nx3] after modification
    :return: none
    '''
    ftp = open(original_path, 'r')
    file_org = ftp.readlines()

    file = open(path, 'w')

    j = 0
    for i in range(0, len(file_org)):
        if file_org[i].startswith('v '):
            data = file_org[i].strip().split(' ')
            file.write('v ' + str(vertices[j][0]) + ' ' + str(vertices[j][1]) + ' ' + str(
                vertices[j][2]) + '\n')
            j = j + 1
        else:
            file.write(file_org[i])

    ftp.close()
    file.close()

def generate_multiple_models(path, output, mod_per_transform):
    '''
    Function used for augment the data and save the output 
    Generates (3 x mod_per_transform + 1) new models.
    :param path: path to the file containing definition of 3D model to be processed
    :param output: path to the folder where output files should be stored
    :param mod_per_transform: number of new models to be generated from a single transformation
    :return: none
    '''
    # Prepare output file name
    file_name = path.split(os.path.sep)[-1]
    output_path = os.path.join(output, file_name)

    # Load model vertices
    points = load_model(path)

    # Scale model
    for i in range(0, mod_per_transform):
        x = random.uniform(0.8, 1.2)
        y = random.uniform(0.8, 1.2)
        z = random.uniform(0.8, 1.2)
        scale_m = np.array([[x, 0, 0], [0, y, 0], [0, 0, z]])
        scaled_output=points.dot(scale_m)
        save_model(path, output_path[:-4] + '_scale' + str(i) + '_aug'+ '.obj', scaled_output)


def process_files(folder_path):
    '''
    Function used to process the file and generate augmented data
    :param folder_path: path to the folder containing 3D model to be processed
    :return: none
    '''
    for category in os.listdir(folder_path):
        for filename in os.listdir(folder_path +"/"+ category + "/"):
            if filename.endswith('scaled_origin.obj'):
                if not filename.endswith('graphcut_scaled_origin.obj') and not filename.endswith('5000_scaled_origin.obj') and not filename.endswith('5000-graphcut_original_scaled_origin.obj') :

                    generate_multiple_models(os.path.join(folder_path,category,filename), os.path.join(folder_path,category), 3)

MODEL_FOLDER = r'D:\Anything_world\whole_pipeline\model-segmentation-pipeline\aaa\1Newfolder'
process_files(MODEL_FOLDER)

