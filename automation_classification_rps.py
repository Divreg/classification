#!/usr/bin/env python
# coding: utf-8

# In[ ]:

activate_this='/home/marta/virtualenvironment/classification/bin/activate_this.py'
exec(open(activate_this).read(), {'__file__': activate_this})
import numpy as np
import pandas as pd
import glob
from math import *
import os
from os.path import join
from os import listdir
import keras
from tensorflow.keras import layers
from tensorflow.keras import Model
from keras.layers import Dense, Dropout, Activation,Flatten, Convolution2D,InputLayer, MaxPooling2D
import tensorflow as tf
from keras.models import Sequential
from tensorflow.keras.optimizers import RMSprop,Adam
from keras.preprocessing import image


def model(trained_model_path):
    #loading trained model
    loaded_model = tf.keras.models.load_model(trained_model_path)
    loaded_model.layers[0].input_shape 
    return loaded_model


# In[ ]:


def classes(folder,loaded_model):
    count=0
    from keras.preprocessing import image

    classess=[]
    imagePaths=[]

    for category in os.listdir(folder):
        if '.' not in category:

            for filename in os.listdir(folder +"/"+ category + "/"):
           
                file_ =  folder +"/"+category + "/" + filename
            
                if file_.endswith('y_0.png') or  file_.endswith('y_15.png') or  file_.endswith('y_30.png') or  file_.endswith('y_45.png') or  file_.endswith('y_60.png') or  file_.endswith('y_75.png') or  file_.endswith('y_90.png') or  file_.endswith('y_105.png') or  file_.endswith('y_120.png') or  file_.endswith('y_135.png') or  file_.endswith('y_150.png') or  file_.endswith('y_165.png') or  file_.endswith('y_180.png') or  file_.endswith('y_195.png') or  file_.endswith('y_210.png') or  file_.endswith('y_225.png') or  file_.endswith('y_240.png') or  file_.endswith('y_255.png') or  file_.endswith('y_270.png') or  file_.endswith('y_285.png') or  file_.endswith('y_300.png') or  file_.endswith('y_315.png') or  file_.endswith('y_330.png') or file_.endswith('y_345.png'):
                    imagePaths.append(file_)
                    count=count+1
                    #loading the grayscale image
                    test_image = image.load_img(file_, target_size = (150, 150), color_mode='grayscale') 

                    test_image = image.img_to_array(test_image)
                    test_image = np.expand_dims(test_image, axis = 0)

                    #predict the result
                    result = loaded_model.predict(test_image)
                    classes = np.argmax(result, axis = 1)
                    classess.append(classes)
    return classess, imagePaths


# In[ ]:


def output_prefab(classified_classes,images_path,category):
    #giving labels to the output
    rotated=[0,105,120,135,15,150,165,180,195,210,225,240,255,270,285,30,300,315,330,345,45,60,75,90]

    cate_rotated=[]
    categories_rotated=[]
    for r in category:
        for m in rotated:
            cat_rot= str(m)+'_'+str(r)

            if cat_rot not in cate_rotated:
                cate_rotated.append(cat_rot)
    for k in classified_classes:
        for cc in k:
            for n, i in enumerate(cate_rotated):

                if int(cc) == int(n): 
                    categories_rotated.append(cate_rotated[n])
    output=dict(zip(images_path,categories_rotated))
    return output
    

    


# In[ ]:


def prefab_classification(output_class):
    #cleaning the output to find result for classification
    
    df=pd.DataFrame(output_class.items())
    df.columns=["Full_File_path","Angle_category"]
    df['Angle']=df['Angle_category'].str.split('_').str[0]
    df['Category']=df['Angle_category'].str.split('_').str[1]
    df['type']=df['Angle_category'].str.split('_').str[-1]
    df['behaviour']=df['Angle_category'].str.split('_').str[-2]
    df['img_path1']=df['Full_File_path'].str.split('_').str[-1]
    df['img_path2']=df['Full_File_path'].str.split('_').str[-2]
    df['img_path12']=df[['img_path2','img_path1']].agg('_'.join,axis=1)
    df['img_path12'] = [a.replace(b, ' ').strip() for a, b in zip(df['Full_File_path'], df['img_path12'])]
    df['img_path12'] = df['img_path12'].map(lambda x: x.rstrip('_'))
    df['obj']='obj'
    df['img_path']=df[['img_path12','obj']].agg('.'.join,axis=1)
    df['Rotation'] = df['Full_File_path'].str.split('_').str[-1]
    df['Rotation'] = df['Rotation'].str.split('.').str[0]
    df = df.apply(pd.to_numeric, errors='ignore')
    df['degree']=df['Rotation']-df['Angle']
    df['segmentation']=df[['Category','behaviour']].agg('_'.join,axis=1)
    df['prefab']=df[['segmentation','type']].agg('_'.join,axis=1)

    df1=df.groupby("img_path")['prefab'].apply(lambda x:x.values.tolist()).to_dict()
    path=list(df1.keys())
    df2=df.groupby("img_path")['degree'].apply(lambda x:x.values.tolist()).to_dict()  
    
    return df1,df2,path


# In[ ]:


def angle(degree_path):
    #converting all the negative angles to (0,360) instead of -15,-30 etc
    negative_to_positive_conversion={}
    for i,j in degree_path.items():
        m = [360+x if x<0 else x for x in j]
        negative_to_positive_conversion[i] = []
        negative_to_positive_conversion[i].append(m)
    return negative_to_positive_conversion


# In[ ]:


def angle_fine_tuning(angle_items):
    #fine tuning angle by considering 315 to 45 degree as 0 inorder to identify the rotation easily
    fine_tuned_angle={}
    for i,q in angle_items.items():
        for k1 in q:
            
            ff1=dict((x,k1.count(x)) for x in set(k1) if x>=315 or x<=45)
            first_angle=sum(ff1.values())
            fine_tuned_angle.setdefault(i, []).append(0)
            fine_tuned_angle.setdefault(i, []).append(first_angle)

            ff2=dict((x,k1.count(x)) for x in set(k1) if  x>=45 and x<=135)
            second_angle=sum(ff2.values())
            fine_tuned_angle.setdefault(i, []).append(90)
            fine_tuned_angle.setdefault(i, []).append(second_angle)

            ff3=dict((x,k1.count(x)) for x in set(k1) if  x>=135 and x<=225)
            third_angle=sum(ff3.values())
            fine_tuned_angle.setdefault(i, []).append(180)
            fine_tuned_angle.setdefault(i, []).append(third_angle)

            ff4=dict((x,k1.count(x)) for x in set(k1) if  x>=225 and x<=315)
            forth_angle=sum(ff4.values())  
            fine_tuned_angle.setdefault(i, []).append(270)
            fine_tuned_angle.setdefault(i, []).append(forth_angle)
    return fine_tuned_angle
    


# In[ ]:


def angle_threshold(rotate_items):
    #giving threshold to find the angle
    above_threshold={}
    below_threshold={}
    for i,j in rotate_items.items():
        k=max(j[::-2])
        if k>=6:
            kk=j.index(k)-1
            o=j[kk]
            above_threshold[i]=[]
            above_threshold[i].append(o)
        else:
            kk=j.index(k)-1
            o=j[kk]
            below_threshold[i]=[]
            below_threshold[i].append(o)
    
    return above_threshold,below_threshold


# In[ ]:


def prefab_non_prefab_classification(dict_prefab):
    #counting the number of occurance of correctly identifying the prefab and given 0.5 as threshold
    prefab={}
    
    for i,j in dict_prefab.items():
        sum1=len(j)
        kkk=max(set(j), key=j.count)
        for x in set(j):
            kk=(j.count(x))/sum1
            if kk>0.5:
                if x not in prefab:
                    prefab[i] = []
                    prefab[i].append(x)
            
    return prefab


# In[ ]:


def cannot_create_prefab(data_path,prefabs):
    #cannot identify the prefab as threshold is less than 0.5
    no_prefab=[]
    for i in data_path:
        if i not in prefabs :
            no_prefab.append(i)
    return no_prefab


# In[ ]:


def segmented_output(prefab_segmentation):
    #combing output to make a list to pass for segmentation
    segmentation={}
    for x, y in prefab_segmentation.items():
        for i in y:
            if str(i)=='quadruped_ungulate_walk' or str(i)=='quadruped_fat_walk' or str(i)=='quadruped_normal_walk' or str(i)=='quadruped_biped_walk':
                segmentation[x] = []
                segmentation[x].append('quadrupeds')   
            elif str(i)=='winged_standing_walk' or str(i)=='winged_standing_hop':
                segmentation[x] = []
                segmentation[x].append('birds')   
            elif str(i)=='multileg_flyer_fly' or str(i)=='multileg_crawler_walk':
                segmentation[x] = []
                segmentation[x].append('insects')   
            else:
                segmentation[x] = []
                segmentation[x].append(i)  
            
    return segmentation       


# In[ ]:


def writing_files(folder,prefab_files,no_prefab_files,rotation_files,seg_files,prefabs,rotated,segmentation,non_rotation,no_prefab):
    file_path_prefab= open(folder+'/'+prefab_files + '.txt', 'w')
    for x, y in prefabs.items():
        for i in y:
            file_path_prefab.write(x+' '+ str(i)+'\n')
    file_path_prefab.close()
    
    file_path_rotation= open(folder+'/'+rotation_files + '.txt', 'w')
    for x, y in rotated.items():
        for i in y:
            file_path_rotation.write(x+' '+ str(i)+'\n')
    file_path_rotation.close()
    
    file_path_segmemtation= open(folder+'/'+seg_files + '.txt', 'w')
    for x, y in segmentation.items():
        for i in y:
            file_path_segmemtation.write(x+' '+ str(i)+'\n')
    file_path_segmemtation.close()
    
    file_path_no_prefab= open(folder+'/'+no_prefab_files + '.txt', 'w')
    for x in no_prefab:
        file_path_no_prefab.write(x+'\n')
    file_path_no_prefab.close()

    return file_path_rotation, file_path_prefab, file_path_no_prefab, file_path_segmemtation
    



def classification_rotation_prefab(unsegmented_folder_path, trained_model_path):
    categories = ['biped_human_walk', 'multileg_crawler_walk', 'multileg_flyer_fly', 'quadruped_biped_walk',
                  'quadruped_fat_crawl', 'quadruped_fat_walk', 'quadruped_hopper_hop', 'quadruped_normal_walk',
                  'quadruped_ungulate_walk', 'vehicle_four_wheel', 'vehicle_two_wheel', 'winged_flyer_fly',
                  'winged_standing_hop', 'winged_standing_walk']


    saved_model=model(trained_model_path)
    output_classes=classes(unsegmented_folder_path,saved_model)
    prefab_dict=output_prefab(output_classes[0],output_classes[1],categories)
    prefab=prefab_classification(prefab_dict)
    rotation_classification=angle(prefab[1])
    fine_tuning_degree=angle_fine_tuning(rotation_classification)
    threshold_angle=angle_threshold(fine_tuning_degree)
    class_prefab=prefab_non_prefab_classification(prefab[0])
    no_prefabs=cannot_create_prefab(prefab[2],class_prefab)
    segmented_result=segmented_output(class_prefab)
    return writing_files(unsegmented_folder_path,'prefab_list','no_prefab_list','rotation_list','segmentation_list',
                         class_prefab,threshold_angle[0],segmented_result,threshold_angle[1],no_prefabs)

